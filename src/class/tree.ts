class Tree implements ITree {
    trunk: Trunk;

    constructor(trunk?: Trunk) {
        this.trunk = trunk;
    }

    draw(): void {
        this.trunk.draw();
    }
    
    toJSON(): JSON {
        throw new Error("Method not implemented.");
    }
}