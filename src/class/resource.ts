class Resource implements IResource, ISavavable {
    quantity: number;
    quantityMax: number;
    production: number;

    constructor(quantity: number, quantityMax: number, production: number) {
        this.quantity = quantity;
        this.quantityMax = quantityMax;
        this.production = production;
    }

    getQuantity(): number {
        return roundNumber(this.quantity, 0);
    }

    getQuantityMax(): number {
        return roundNumber(this.quantityMax, 0);
    }

    getProduction(): number {
        return roundNumber(this.production, 2);
    }

    add(quantity: number): void {
        this.quantity += quantity;
        if (this.quantityMax && this.quantity > this.quantityMax) {
            this.quantity = this.quantityMax;
        }
    }

    substract(quantity: number): void {
        this.quantity -= quantity;
        if (this.quantity < 0) {
            this.quantity = 0;
        }
    }

    canAfford(quantity: number): boolean {
        return this.quantity >= quantity;
    }

    tick(timeElapsed: number): void {
        this.add(this.production * timeElapsed);
    }

    toJSON(): JSON {
        throw new Error("Method not implemented.");
    }
}