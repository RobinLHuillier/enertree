class Leaf implements ILeaf {
    position: Position;
    visual: Visual;
    ctx: CanvasRenderingContext2D = ctxTreeLeaves;

    constructor(position?: Position, visual?: Visual) {
        this.position = position;
        this.visual = visual;
    }

    refreshAnimation(): void {
        this.visual.currentTick++;
        if (this.visual.currentTick >= this.visual.ticksPerFrame) {
            this.visual.currentTick = 0;
            this.visual.currentFrame++;
            if (this.visual.currentFrame >= this.visual.src.length) 
                this.visual.currentFrame = 0;
        }
    }

    draw(): void {
        this.ctx.drawImage(this.visual.src[this.visual.currentFrame ?? 0], this.position.x, this.position.y);

        this.refreshAnimation();
    }
    
    toJSON(): JSON {
        throw new Error("Method not implemented.");
    }
}