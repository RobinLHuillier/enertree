class Twig implements ITwig {
    position: Position;
    visual: Visual;
    size: number;
    ctx: CanvasRenderingContext2D = ctxTreeTrunk;
    leaves: ILeaf[] = [];

    constructor(size?: number, position?: Position, visual?: Visual) {
        this.position = position;
        this.visual = visual;
        this.size = 4;
    }

    canGrowLeaf(): boolean {
        return this.leaves.length < this.size;
    }

    addLeaf(): Leaf|null {
        if (this.leaves.length < this.size) {
            let leaf = new Leaf();

            const direction = this.calculateLeafDirection();
            const position = this.calculateLeafPosition(direction);
            leaf.position = position;
            const images = [
                [img['leaf-left-1'], img['leaf-left-2']],
                [img['leaf-right-1'], img['leaf-right-2']],
                [img['leaf-top-1'], img['leaf-top-2']],
            ]
            const choice = randElem(images);
            const visual: Visual = {
                src: choice,
                currentFrame: 0,
                ticksPerFrame: 10,
                currentTick: rand(0, 10),
            }; 
            leaf.visual = visual;

            this.leaves.push(leaf);
            return leaf;
        }
        return null;
    }

    calculateLeafDirection(): string {
        return randElem(['left', 'right', 'top']);
    }

    calculateLeafPosition(direction: string): Position {
        const widthLeaf = img['leaf-left-1'].width;
        const offsetLeafX = direction === 'left' ? -widthLeaf : direction === 'right' ? 0 : -widthLeaf / 2;
        const offsetLeafY = direction === 'top' ? -widthLeaf : -widthLeaf / 2;

        let offsetXTwig = 0;
        let offsetYTwig = 0;
        const sizeTwig = this.visual.src[0].width;
        const imgName = this.visual.src[0].src.substring(this.visual.src[0].src.lastIndexOf('/') + 1);
        switch (imgName) {
            case 'twig-1.png':
                offsetYTwig = sizeTwig;
                break;
            case 'twig-2.png':
                break;
            case 'twig-3.png':
                offsetXTwig = sizeTwig;
                break;
            case 'twig-4.png':
                offsetXTwig = sizeTwig;
                offsetYTwig = sizeTwig;
                break;
            default:
                console.error('calculateLeafPosition: twig not found');
                break;
        }

        return {
            x: this.position.x + offsetLeafX + offsetXTwig,
            y: this.position.y + offsetLeafY + offsetYTwig,
        };
    }

    draw(): void {
        this.ctx.drawImage(this.visual.src[this.visual.currentFrame ?? 0], this.position.x, this.position.y);

        for (let leaf of this.leaves) {
            leaf.draw();
        }
    }
    
    toJSON(): JSON {
        throw new Error("Method not implemented.");
    }
}