class Branch implements IBranch {
    size: number;
    sizeMax: number = 4;
    twigs: Twig[];
    ctx: CanvasRenderingContext2D = ctxTreeTrunk;
    position: Position;
    visual: Visual;

    constructor(size?: number, position?: Position, visual?: Visual) {
        this.size = 1;
        this.twigs = [];
        this.position = {x: 100, y: 500};
    }

    canGrow(): boolean {
        return this.size < this.sizeMax;
    }

    canGrowTwig(): boolean {
        return this.twigs.length < this.size;
    }

    canGrowLeaf(): boolean {
        return this.twigs.some(twig => twig.canGrowLeaf());
    }

    grow(): void {
        if (this.canGrow()) {
            this.size++;

            const imgName = this.visual.src[0].src;
            const direction = imgName.includes('right') ? 'right' : 'left';
            this.visual.src[0] = img[`branch-${direction}-${this.size}`];
        }
    }

    addTwig(): Twig|null {
        if (this.canGrowTwig()) {
            let twig = new Twig();

            const imgName = this.calculateImageName();
            const position = this.calculatePositionNewTwig(imgName);

            // console.log(position, this.position);
            twig.position = position;
            twig.visual = {src: [img[imgName]]};

            this.twigs.push(twig);
            return twig;
        }
        return null;
    }

    calculateImageName(): string {
        if (this.twigs.length === 0) {
            if (this.visual.src[0].src.includes('left')) {
                return randElem(['twig-1', 'twig-3']);
            } else {
                return randElem(['twig-2', 'twig-4']);
            }
        }
        const lastImg = this.twigs[this.twigs.length - 1].visual.src[0].src;
        const lastImgName = lastImg.substring(lastImg.lastIndexOf("/") + 1);

        switch(lastImgName) {
            case 'twig-1.png':
                return 'twig-3';
            case 'twig-2.png':
                return 'twig-4';
            case 'twig-3.png':
                return 'twig-1';
            case 'twig-4.png':
                return 'twig-2';
            default:
                console.error('calculateImageName() failed', lastImgName);
                return 'twig-1';
        }
    }

    calculatePositionNewTwig(imgName: string): Position {
        const myDirection = this.visual.src[0].src.includes('left') ? 'left' : 'right';
        const myWidth = this.visual.src[0].width;
        const myHeight = this.visual.src[0].height;
        const originalOffsetX = myDirection === 'left' ?  myWidth : 0;
        const originalOffsetY = myHeight;
        
        const twigCount = this.twigs.length+1;
        const offsetY = -(twigCount-0.5) * myWidth / 4; 
        const offsetX = myDirection === 'left' ? offsetY : -offsetY;

        const sizeTwig = img[imgName].width;
        let offsetXTwig = 0;
        let offsetYTwig = 0;
        switch(imgName) {
            case 'twig-1':
                offsetYTwig = -sizeTwig;
                break;
            case 'twig-2':
                break;
            case 'twig-3':
                offsetXTwig = -sizeTwig;
                break;
            case 'twig-4':
                offsetXTwig = -sizeTwig;
                offsetYTwig = -sizeTwig;
                break;
            default:
                console.error('calculatePositionNewTwig() failed');
                break;
        }

        const semiWidth = myWidth / 8;
        const wiggle = rand(-semiWidth, semiWidth);

        const theWorstY = myDirection === 'right' ? 0: 0;
        const theWorstX = myDirection === 'right' ? 0: 0;
        // console.log(imgName, offsetXTwig, offsetYTwig, offsetX, offsetY, wiggle);

        return {
            x: this.position.x + originalOffsetX + wiggle + offsetX + offsetXTwig + theWorstX,
            y: this.position.y + originalOffsetY + wiggle + offsetY + offsetYTwig + theWorstY,
        }
    }

    addLeaf(): Leaf|null {
        for (let twig of shuffleArray(this.twigs)) {
            let leaf = twig.addLeaf();
            if (leaf != null) {
                return leaf;
            }
        }
        return null;
    }

    draw(): void {
        this.ctx.drawImage(this.visual.src[this.visual.currentFrame ?? 0], this.position.x, this.position.y);

        for (let twig of this.twigs) {
            twig.draw();
        }
    }

    toJSON(): JSON {
        throw new Error("Method not implemented.");
    }
}