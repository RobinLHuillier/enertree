class Button {
    x: number;
    y: number;
    w: number;
    h: number;
    ctx: CanvasRenderingContext2D;
    lineWidth: number;
    colorStroke: Color|null;
    colorFill: Color|null;
    colorText: Color|null;
    sizeText: number;
    text: string;
    xText: number = 4;
    title: string|null;
    colorHover: Color|null;
    colorFillActive: Color|null;
    colorStrokeActive: Color|null;
    colorTextActive: Color|null;
    visible: boolean = true;
    activable: boolean = true;
    isHovered: boolean;
    active: boolean;

    constructor(
        x: number,
        y: number,
        w: number,
        h: number,
        ctx: CanvasRenderingContext2D,
        lineWidth: number,
        colorStroke: Color|null,
        colorFill: Color|null,
        colorText: Color|null,
        sizeText: number,
        text: string,
        xText: number = 4,
        title: string|null,
        colorHover: Color|null,
        colorFillActive: Color|null,
        colorStrokeActive: Color|null,
        colorTextActive: Color|null,
        visible: boolean = true,
        activable: boolean = true
    ) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.ctx = ctx;
        this.lineWidth = lineWidth;
        this.colorStroke = colorStroke;
        this.colorFill = colorFill;
        this.colorText = colorText;
        this.sizeText = sizeText;
        this.text = text;
        this.xText = this.x+xText;
        this.title = title;
        this.colorHover = colorHover;
        this.isHovered = false;
        this.colorFillActive = colorFillActive;
        this.colorStrokeActive = colorStrokeActive;
        this.colorTextActive = colorTextActive;
        this.active = false;
        this.visible = visible;
        this.activable = activable;
    }

    amIFocused(position?: Position): boolean {
        return !(!this.visible || undefined === position ||position.x < this.x || position.x > this.x+this.w || position.y < this.y || position.y > this.y+this.h);
    }

    click(position: Position): string|null {
        if (!this.amIFocused(position)) {
            return null;
        }
        if (this.activable) {
            this.active = true;
        }
        return this.title;
    }

    hover(position?): boolean {
        if (!this.amIFocused(position)) {
            this.isHovered = false;
            return false;
        }
        this.isHovered = true;
        return true;
    }

    unactive(): void {
        this.active = false;
    }

    setInvisible(): void {
        this.visible = false;
    }

    setVisible(): void {
        this.visible = true;
    }

    draw(position?): void {
        if (!this.visible) {
            return;
        }
        this.hover(position);
        this.ctx.lineWidth = this.lineWidth;
        roundRect(this.ctx, this.x, this.y, this.w, this.h, this.active ? this.colorStrokeActive : this.colorStroke, this.active ? this.colorFillActive : this.isHovered ? this.colorHover : this.colorFill, 5);
        this.ctx.font = this.sizeText.toString() + "px Arial";
        this.ctx.fillStyle = this.active ? this.colorTextActive : this.colorText;
        this.ctx.fillText(this.text, this.xText, this.y+this.sizeText+6, this.w-8);
    }
}
