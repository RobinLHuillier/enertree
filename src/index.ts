var gameHandler: GameHandler;
var menuHandler: MenuHandler;
var resourceHandler: ResourceHandler;
var treeHandler: TreeHandler;

function initHandlers(): void {
    menuHandler = new MenuHandler();
    resourceHandler = new ResourceHandler();
    treeHandler = new TreeHandler();
}

document.addEventListener('mousemove', e => {
    canvasPosition = canvasFront.getBoundingClientRect();
    mouse.x = Math.round(e.x - canvasPosition.left);
    mouse.y = Math.round(e.y - canvasPosition.top);
});

document.addEventListener('mousedown', e => {mouseDown()});
document.addEventListener('mouseup', e => {mouseUp()});

function mouseDown(): void {
    // gameHandler.mouseDown();
}

function mouseUp(): void {
    gameHandler.click();
}

function animate(timestamp: DOMHighResTimeStamp): void {
    if(lastTime == 0) lastTime = timestamp-1;
    if(timestamp - lastTime >= refresh || timestamp - lastTime == 0) {
        if(!gameHandler.amIWorking) {
            // console.time("core");
            gameHandler.core();
            // console.timeEnd("core");
            timer += timestamp - lastTime;
            lastTime = timestamp;
        }
    }
    window.requestAnimationFrame(animate);
}

window.onload = function(): void {
    ctxFront.clearRect(0,0,canvasFront.width,canvasFront.height);
    ctxBackground.fillStyle = yellow;
    ctxBackground.fillRect(0,0,canvasBackground.width,canvasBackground.height);
    initHandlers();
    gameHandler = new GameHandler();
    gameHandler.initialize().finally(() => {
        animate(performance.now());
    });
}
