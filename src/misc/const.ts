//canvas layers
const canvasBackground: HTMLCanvasElement = <HTMLCanvasElement>document.getElementById('canvasBackground');
const ctxBackground: CanvasRenderingContext2D = canvasBackground.getContext('2d');

const canvasTreeTrunk: HTMLCanvasElement = <HTMLCanvasElement>document.getElementById('canvasTreeTrunk');
const ctxTreeTrunk: CanvasRenderingContext2D = canvasTreeTrunk.getContext('2d');
const canvasTreeLeaves: HTMLCanvasElement = <HTMLCanvasElement>document.getElementById('canvasTreeLeaves');
const ctxTreeLeaves: CanvasRenderingContext2D = canvasTreeLeaves.getContext('2d');

const canvasFront: HTMLCanvasElement = <HTMLCanvasElement>document.getElementById('canvasFront');
const ctxFront: CanvasRenderingContext2D = canvasFront.getContext('2d');

//mouse
var canvasPosition: DOMRect = canvasFront.getBoundingClientRect();
var mouse: {x: number,  y: number} = {
    x: undefined,
    y: undefined,
};

//time
const refresh = 50;
const tickPerSec = 1000/refresh;
const infSymbol = "∞";
var lastTime: number = 0;
var timer: number;

//img
const img: {[key: string]: HTMLImageElement|null} = {
    "leaf-left-1" : null,
    "leaf-left-2" : null,
    "leaf-right-1" : null,
    "leaf-right-2" : null,
    "leaf-top-1" : null,
    "leaf-top-2" : null,

    "branch-left-1" : null,
    "branch-left-2" : null,
    "branch-left-3" : null,
    "branch-left-4" : null,
    "branch-right-1" : null,
    "branch-right-2" : null,
    "branch-right-3" : null,
    "branch-right-4" : null,

    "twig-1": null,
    "twig-2": null,
    "twig-3": null,
    "twig-4": null,
};
const sound = {};

//colors
// https://lospec.com/palette-list/retrocal-8 
// by polyfrog
const cyan = "rgb(110, 184, 168)" as const;
const darkGreen = "rgb(42, 88, 79)" as const;
const green = "rgb(116, 163, 63)" as const;
const yellow = "rgb(252, 255, 192)" as const;
const red = "rgb(198, 80, 90)" as const;
const dark = "rgb(47, 20, 47)" as const;
const brown = "rgb(119, 68, 72)" as const;
const orange = "rgb(238, 156, 93)" as const;
