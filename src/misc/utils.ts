function rand(max: number, min?: number): number {
    if (undefined === min) {
        min = 0;
    }
    return Math.floor(Math.random() * (max-min)) + min;
}

function randElem<T>(array: T[]): T {
    return array[rand(array.length)];
}

function addFade(color: Color, fade: number): string {
    return color.slice(0,-1) + "," + fade.toString() + ")";
}

function delElem<T>(array: T[], elem: T): void {
    if (array.indexOf(elem) === -1) {
        console.error("[delElem] trying to delete an element non existent", array, elem);
        return;
    }
    array.splice(array.indexOf(elem), 1);
}

function shuffleArray<T>(array: T[]): T[] {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
}

function roundRect(
    ctx: CanvasRenderingContext2D, 
    x: number, 
    y: number, 
    width: number, 
    height: number, 
    colorStroke: Color, 
    colorFill: Color, 
    radius: number = 5
): void {
    ctx.beginPath();
    ctx.roundRect(x, y, width, height, radius);
    ctx.closePath();
    ctx.strokeStyle = colorStroke;
    ctx.fillStyle = colorFill;
    ctx.fill();
    ctx.stroke();
}

function roundNumber(num: number, decimals: number): number {
    return Math.round(num * Math.pow(10, decimals)) / Math.pow(10, decimals);
}