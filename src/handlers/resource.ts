class ResourceHandler implements IHandler {
    energy: Resource;
    matter: Resource;
    seeds: Resource;

    initialize(): void {
        this.energy = new Resource(0, 10, 1);
        this.matter = new Resource(0, 10, 1);
        this.seeds = new Resource(0, 0, 1);
    }

    readStore(): void {

    }

    click(): boolean {
        throw new Error("Method not implemented.");
    }

    draw(): void {
        // throw new Error("Method not implemented.");
    }

    core(): void {
        // throw new Error("Method not implemented.");
    }
}