class GameHandler implements IHandler {
    amIWorking: boolean = false;

    async initialize(): Promise<void> {
        await this.loadImages();

        menuHandler.initialize();
        resourceHandler.initialize();
        treeHandler.initialize();
    }

    async loadImages(): Promise<any[]> {
        const promiseArray = [];
    
        for (let key of Object.keys(img)) {
            promiseArray.push(new Promise<void>(resolve => {
                img[key] = new Image();
                img[key].onload = () => {
                    resolve();
                };
                img[key].src = "assets/img/"+key+".png";
            }));
        }

        return Promise.all(promiseArray);
    }

    click(): boolean {
        if (menuHandler.click()) {
            return true;
        }

        return false;
    }

    draw() {
        menuHandler.draw();
        resourceHandler.draw();
        treeHandler.draw();
    }

    core() {
        menuHandler.core();
        resourceHandler.core();
        treeHandler.core();

        this.draw();
    }
}
