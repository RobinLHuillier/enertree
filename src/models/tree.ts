interface ITree extends ISavavable {
    trunk: ITrunk;

    draw(): void;
}