interface TreePart {
    name: TreePartName;
    cost: ResourceCost[];
    costIncrement: number;
    costMultiplier: number;
    quantity: number;
}