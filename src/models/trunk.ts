interface ITrunk extends ISavavable, IDrawable {
    size: number;
    branches: IBranch[];
}