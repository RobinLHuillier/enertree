interface IResource {
    quantity: number;
    quantityMax: number;
    production: number;
}