interface IHandler {
    initialize(): void;
    click(): boolean;
    draw(): void;
    core(): void;
}