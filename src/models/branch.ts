interface IBranch extends ISavavable, IDrawable {
    size: number;
    twigs: ITwig[];
}