interface IDrawable {
    ctx: CanvasRenderingContext2D;
    position: Position;
    visual: Visual;
    needRedraw?: boolean;
    draw(): void;
}