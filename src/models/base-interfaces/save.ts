interface Save {
    treeParts: Partial<Record<TreePartName, TreePart>>;
    resources: Partial<Record<ResourceName, IResource>>;
}