type Visual = {
    src?: HTMLImageElement[];

    nbFrames?: number;
    currentFrame?: number;
    ticksPerFrame?: number;
    currentTick?: number;
    
    color?: Color;
}