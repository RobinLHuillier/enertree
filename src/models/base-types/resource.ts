type ResourceName = "energy" | "matter" | "seeds";

type ResourceCost = [ResourceName, number];