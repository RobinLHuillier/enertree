**12/07/23: FIRST DAY**

*starting block*
X-> installation typescript
X-> installation framework
X-> adaptation ts
X-> récupérer les colors

*first visuals*
X-> positionner deux canvas pour l'arbre
X-> model pour tree / tige / feuille
X-> treeHandler : afficher tige en fonction d'une taille
X-> trouver ou faire des models de feuille, par série pour les animer
X-> animer les feuilles

*tree generation*
X-> models de tige: trunk, branch, twig, leaf
X-> ordre de draw : trunk, branch, twig, pas la peine de redraw à chaque tick
    X-> pas obligé, à pas l'air couteux en fait
X-> classes pour chaque, avec calcul de position, de combien d'éléments, etc
X-> ajouter les twigs avec offset, image, etc.
X-> afficher feuilles en fonction nombre actuel + taille de tige (pas juste de haut en bas, des positions)
    X-> position en fonction de la tige

*first tests*
X-> récupérer classe boutons
X-> ajouter des boutons au menu pour tester les arbres


**13/07/23: SECOND DAY**

*first gameplay*
X-> réfléchir aux ressources, comment elles se mettent en place, comment elles sont utilisées
X-> établir des modèles de coûts pour les ressources, marquer le minimum à faire
X-> models pour resources / treeparts
X-> lire dans le store les treeparts et créer les classes correspondantes
-> lire dans le store les resources et créer les classes correspondantes

-> calcul de difference de temps entre chaque tick (difference réelle)
-> soleil fait gagner de l'énergie, gestion énergie (stockage)

-> affichage stylé menu production
-> affichage de l'énergie/matière actuelle, de la capacité de stockage
-> affichage de la vitesse de production d'énergie/matière
-> batiments : trunk, branch, twig, leaf: cout, achat, affichage

*gameplay expansion*
-> flowers: les twigs contiennent maintenant des flowers/leaves
-> flowers: les flowers produisent des seeds, affichage du nombre de seeds
-> roots: les roots stockent de l'énergie et accèlérent la conversion d'énérgie en matière
-> roots: les roots peuvent être ajoutées, affichage des roots



**BUGS**

-> les twigs s'écartent des branches parfois (wiggle ? position ?)
-> les feuilles sont mal calées sur les twigs (position ?)