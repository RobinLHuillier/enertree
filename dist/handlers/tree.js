class TreeHandler {
    initialize() {
        this.clearDraw();
        let trunk = new Trunk();
        this.tree = new Tree(trunk);
        this.readStore();
    }
    readStore() {
        for (let i = 0; i < save.treeParts.trunk.quantity; i++) {
            this.growTrunk();
        }
        for (let i = 0; i < save.treeParts.branch.quantity; i++) {
            this.growBranch();
        }
        for (let i = 0; i < save.treeParts.twig.quantity; i++) {
            this.growTwig();
        }
        for (let i = 0; i < save.treeParts.leaf.quantity; i++) {
            this.growLeaf();
        }
    }
    click() {
        return true;
    }
    clearDraw() {
        ctxTreeLeaves.clearRect(0, 0, canvasTreeLeaves.width, canvasTreeLeaves.height);
        ctxTreeTrunk.clearRect(0, 0, canvasTreeTrunk.width, canvasTreeTrunk.height);
    }
    growTrunk() {
        if (this.tree.trunk.canGrow()) {
            this.tree.trunk.grow();
        }
    }
    growBranch() {
        if (this.tree.trunk.canGrowBranch()) {
            this.tree.trunk.growBranch();
        }
    }
    growTwig() {
        if (this.tree.trunk.canGrowTwig()) {
            this.tree.trunk.addTwig();
        }
    }
    growLeaf() {
        if (this.tree.trunk.canGrowLeaf()) {
            this.tree.trunk.addLeaf();
        }
    }
    draw() {
        ctxTreeLeaves.clearRect(0, 0, canvasTreeLeaves.width, canvasTreeLeaves.height);
        this.tree.draw();
    }
    core() {
    }
}
