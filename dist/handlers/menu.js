class MenuHandler {
    constructor() {
        this.buttons = [];
    }
    initialize() {
        this.buttons = [];
        this.buttons.push(new Button(500, 20, 200, 50, ctxFront, 2, darkGreen, cyan, dark, 30, "Grow Trunk", 5, "grow trunk", green, dark, dark, dark, true, false));
        this.buttons.push(new Button(500, 120, 200, 50, ctxFront, 2, darkGreen, cyan, dark, 30, "Grow Branch", 5, "grow branch", green, dark, dark, dark, true, false));
        this.buttons.push(new Button(500, 220, 200, 50, ctxFront, 2, darkGreen, cyan, dark, 30, "Grow Twig", 5, "grow twig", green, dark, dark, dark, true, false));
        this.buttons.push(new Button(500, 320, 200, 50, ctxFront, 2, darkGreen, cyan, dark, 30, "Grow Leaf", 5, "grow leaf", green, dark, dark, dark, true, false));
    }
    click() {
        for (let i = 0; i < this.buttons.length; i++) {
            const result = this.buttons[i].click(mouse);
            if (result) {
                switch (result) {
                    case "grow trunk":
                        treeHandler.growTrunk();
                        break;
                    case "grow branch":
                        treeHandler.growBranch();
                        break;
                    case "grow twig":
                        treeHandler.growTwig();
                        break;
                    case "grow leaf":
                        treeHandler.growLeaf();
                        break;
                    default:
                        console.error("Unknown button result: " + result);
                        break;
                }
                return true;
            }
        }
        return false;
    }
    draw() {
        for (let button of this.buttons) {
            button.draw(mouse);
        }
    }
    core() {
    }
}
