var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class GameHandler {
    constructor() {
        this.amIWorking = false;
    }
    initialize() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.loadImages();
            menuHandler.initialize();
            resourceHandler.initialize();
            treeHandler.initialize();
        });
    }
    loadImages() {
        return __awaiter(this, void 0, void 0, function* () {
            const promiseArray = [];
            for (let key of Object.keys(img)) {
                promiseArray.push(new Promise(resolve => {
                    img[key] = new Image();
                    img[key].onload = () => {
                        resolve();
                    };
                    img[key].src = "assets/img/" + key + ".png";
                }));
            }
            return Promise.all(promiseArray);
        });
    }
    click() {
        if (menuHandler.click()) {
            return true;
        }
        return false;
    }
    draw() {
        menuHandler.draw();
        resourceHandler.draw();
        treeHandler.draw();
    }
    core() {
        menuHandler.core();
        resourceHandler.core();
        treeHandler.core();
        this.draw();
    }
}
