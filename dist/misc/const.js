//canvas layers
const canvasBackground = document.getElementById('canvasBackground');
const ctxBackground = canvasBackground.getContext('2d');
const canvasTreeTrunk = document.getElementById('canvasTreeTrunk');
const ctxTreeTrunk = canvasTreeTrunk.getContext('2d');
const canvasTreeLeaves = document.getElementById('canvasTreeLeaves');
const ctxTreeLeaves = canvasTreeLeaves.getContext('2d');
const canvasFront = document.getElementById('canvasFront');
const ctxFront = canvasFront.getContext('2d');
//mouse
var canvasPosition = canvasFront.getBoundingClientRect();
var mouse = {
    x: undefined,
    y: undefined,
};
//time
const refresh = 50;
const tickPerSec = 1000 / refresh;
const infSymbol = "∞";
var lastTime = 0;
var timer;
//img
const img = {
    "leaf-left-1": null,
    "leaf-left-2": null,
    "leaf-right-1": null,
    "leaf-right-2": null,
    "leaf-top-1": null,
    "leaf-top-2": null,
    "branch-left-1": null,
    "branch-left-2": null,
    "branch-left-3": null,
    "branch-left-4": null,
    "branch-right-1": null,
    "branch-right-2": null,
    "branch-right-3": null,
    "branch-right-4": null,
    "twig-1": null,
    "twig-2": null,
    "twig-3": null,
    "twig-4": null,
};
const sound = {};
//colors
// https://lospec.com/palette-list/retrocal-8 
// by polyfrog
const cyan = "rgb(110, 184, 168)";
const darkGreen = "rgb(42, 88, 79)";
const green = "rgb(116, 163, 63)";
const yellow = "rgb(252, 255, 192)";
const red = "rgb(198, 80, 90)";
const dark = "rgb(47, 20, 47)";
const brown = "rgb(119, 68, 72)";
const orange = "rgb(238, 156, 93)";
