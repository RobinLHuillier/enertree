function rand(max, min) {
    if (undefined === min) {
        min = 0;
    }
    return Math.floor(Math.random() * (max - min)) + min;
}
function randElem(array) {
    return array[rand(array.length)];
}
function addFade(color, fade) {
    return color.slice(0, -1) + "," + fade.toString() + ")";
}
function delElem(array, elem) {
    if (array.indexOf(elem) === -1) {
        console.error("[delElem] trying to delete an element non existent", array, elem);
        return;
    }
    array.splice(array.indexOf(elem), 1);
}
function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
}
function roundRect(ctx, x, y, width, height, colorStroke, colorFill, radius = 5) {
    ctx.beginPath();
    ctx.roundRect(x, y, width, height, radius);
    ctx.closePath();
    ctx.strokeStyle = colorStroke;
    ctx.fillStyle = colorFill;
    ctx.fill();
    ctx.stroke();
}
function roundNumber(num, decimals) {
    return Math.round(num * Math.pow(10, decimals)) / Math.pow(10, decimals);
}
