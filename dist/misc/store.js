var save;
resetSave();
function resetSave() {
    save = {
        treeParts: {
            "trunk": {
                cost: [["matter", 10]],
                costIncrement: 0,
                costMultiplier: 3,
                quantity: 1,
            },
            "branch": {
                cost: [["matter", 5]],
                costIncrement: 0,
                costMultiplier: 2.5,
                quantity: 1,
            },
            "twig": {
                cost: [["matter", 2]],
                costIncrement: 0,
                costMultiplier: 2,
                quantity: 1,
            },
            "leaf": {
                cost: [["matter", 1]],
                costIncrement: 1,
                costMultiplier: 1.1,
                quantity: 1,
            },
        },
        resources: {
            "energy": {
                quantity: 0,
                quantityMax: 10,
                production: 1,
            },
            "matter": {
                quantity: 0,
                quantityMax: 10,
                production: 0,
            },
            "seeds": {
                quantity: 0,
                quantityMax: 0,
                production: 0,
            },
        }
    };
}
