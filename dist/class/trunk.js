class Trunk {
    constructor(position, visual) {
        this.sizeMax = 16;
        this.ctx = ctxTreeTrunk;
        this.sizeMultX = 2;
        this.sizeMultY = 25;
        this.position = { x: 100, y: 500 };
        this.visual = { color: brown };
        this.size = 1;
        this.branches = [];
    }
    grow() {
        if (this.canGrow()) {
            this.size++;
        }
    }
    canGrow() {
        return this.size < this.sizeMax;
    }
    canGrowTwig() {
        return this.branches.some(branch => branch.canGrowTwig());
    }
    canGrowLeaf() {
        return this.branches.some(branch => branch.canGrowLeaf());
    }
    canGrowNewBranch() {
        return this.branches.length < this.size;
    }
    canGrowBranch() {
        return this.canGrowNewBranch() || this.branches.some(branch => branch.canGrow());
    }
    growBranch() {
        const growableBranches = [];
        for (let branch of this.branches) {
            if (branch.canGrow()) {
                growableBranches.push(branch);
            }
        }
        if (this.canGrowNewBranch() && (rand(0, 100) < 50) || growableBranches.length === 0) {
            this.addBranch();
            return true;
        }
        if (growableBranches.length === 0) {
            return false;
        }
        const chosenBranch = randElem(growableBranches);
        chosenBranch.grow();
        return true;
    }
    addBranch() {
        if (this.canGrowBranch()) {
            let branch = new Branch();
            const imgName = this.calculateImageName();
            const position = this.calculatePositionNewBranch(imgName);
            branch.position = position;
            branch.visual = { src: [img[imgName]] };
            this.branches.push(branch);
            return branch;
        }
        return null;
    }
    calculateImageName() {
        if (this.branches.length === 0) {
            return randElem(['branch-left-1', 'branch-right-1']);
        }
        const lastImg = this.branches[this.branches.length - 1].visual.src[0].src;
        return lastImg.includes('right') ? 'branch-left-1' : 'branch-right-1';
    }
    calculatePositionNewBranch(imgName) {
        const offsetX = imgName.includes('right') ? 0 : img[imgName].width;
        const branchCount = this.branches.length + 1;
        const offsetY = (branchCount - 0.5) * this.sizeMultY + img[imgName].height;
        const offsetWiggle = branchCount;
        const wiggleX = rand(-offsetWiggle, offsetWiggle);
        const wiggleY = rand(-offsetWiggle, offsetWiggle);
        return {
            x: this.position.x - offsetX + wiggleX,
            y: this.position.y - offsetY + wiggleY,
        };
    }
    addTwig() {
        for (let branch of shuffleArray(this.branches.filter(branch => branch.canGrowTwig()))) {
            let twig = branch.addTwig();
            if (twig != null) {
                return twig;
            }
        }
        return null;
    }
    addLeaf() {
        for (let branch of shuffleArray(this.branches)) {
            for (let twig of shuffleArray(branch.twigs)) {
                let leaf = twig.addLeaf();
                if (leaf != null) {
                    return leaf;
                }
            }
        }
        return null;
    }
    draw() {
        const topLeft = {
            x: this.position.x - this.size * this.sizeMultX,
            y: this.position.y - this.size * this.sizeMultY,
        };
        const width = this.size * this.sizeMultX * 2;
        const height = this.size * this.sizeMultY;
        this.ctx.fillStyle = this.visual.color;
        this.ctx.fillRect(topLeft.x, topLeft.y, width, height);
        for (let branch of this.branches) {
            branch.draw();
        }
    }
    toJSON() {
        throw new Error("Method not implemented.");
    }
}
