class Leaf {
    constructor(position, visual) {
        this.ctx = ctxTreeLeaves;
        this.position = position;
        this.visual = visual;
    }
    refreshAnimation() {
        this.visual.currentTick++;
        if (this.visual.currentTick >= this.visual.ticksPerFrame) {
            this.visual.currentTick = 0;
            this.visual.currentFrame++;
            if (this.visual.currentFrame >= this.visual.src.length)
                this.visual.currentFrame = 0;
        }
    }
    draw() {
        var _a;
        this.ctx.drawImage(this.visual.src[(_a = this.visual.currentFrame) !== null && _a !== void 0 ? _a : 0], this.position.x, this.position.y);
        this.refreshAnimation();
    }
    toJSON() {
        throw new Error("Method not implemented.");
    }
}
