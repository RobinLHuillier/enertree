class Twig {
    constructor(size, position, visual) {
        this.ctx = ctxTreeTrunk;
        this.leaves = [];
        this.position = position;
        this.visual = visual;
        this.size = 4;
    }
    canGrowLeaf() {
        return this.leaves.length < this.size;
    }
    addLeaf() {
        if (this.leaves.length < this.size) {
            let leaf = new Leaf();
            const direction = this.calculateLeafDirection();
            const position = this.calculateLeafPosition(direction);
            leaf.position = position;
            const images = [
                [img['leaf-left-1'], img['leaf-left-2']],
                [img['leaf-right-1'], img['leaf-right-2']],
                [img['leaf-top-1'], img['leaf-top-2']],
            ];
            const choice = randElem(images);
            const visual = {
                src: choice,
                currentFrame: 0,
                ticksPerFrame: 10,
                currentTick: rand(0, 10),
            };
            leaf.visual = visual;
            this.leaves.push(leaf);
            return leaf;
        }
        return null;
    }
    calculateLeafDirection() {
        return randElem(['left', 'right', 'top']);
    }
    calculateLeafPosition(direction) {
        const widthLeaf = img['leaf-left-1'].width;
        const offsetLeafX = direction === 'left' ? -widthLeaf : direction === 'right' ? 0 : -widthLeaf / 2;
        const offsetLeafY = direction === 'top' ? -widthLeaf : -widthLeaf / 2;
        let offsetXTwig = 0;
        let offsetYTwig = 0;
        const sizeTwig = this.visual.src[0].width;
        const imgName = this.visual.src[0].src.substring(this.visual.src[0].src.lastIndexOf('/') + 1);
        switch (imgName) {
            case 'twig-1.png':
                offsetYTwig = sizeTwig;
                break;
            case 'twig-2.png':
                break;
            case 'twig-3.png':
                offsetXTwig = sizeTwig;
                break;
            case 'twig-4.png':
                offsetXTwig = sizeTwig;
                offsetYTwig = sizeTwig;
                break;
            default:
                console.error('calculateLeafPosition: twig not found');
                break;
        }
        return {
            x: this.position.x + offsetLeafX + offsetXTwig,
            y: this.position.y + offsetLeafY + offsetYTwig,
        };
    }
    draw() {
        var _a;
        this.ctx.drawImage(this.visual.src[(_a = this.visual.currentFrame) !== null && _a !== void 0 ? _a : 0], this.position.x, this.position.y);
        for (let leaf of this.leaves) {
            leaf.draw();
        }
    }
    toJSON() {
        throw new Error("Method not implemented.");
    }
}
