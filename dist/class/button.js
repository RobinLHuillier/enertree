class Button {
    constructor(x, y, w, h, ctx, lineWidth, colorStroke, colorFill, colorText, sizeText, text, xText = 4, title, colorHover, colorFillActive, colorStrokeActive, colorTextActive, visible = true, activable = true) {
        this.xText = 4;
        this.visible = true;
        this.activable = true;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.ctx = ctx;
        this.lineWidth = lineWidth;
        this.colorStroke = colorStroke;
        this.colorFill = colorFill;
        this.colorText = colorText;
        this.sizeText = sizeText;
        this.text = text;
        this.xText = this.x + xText;
        this.title = title;
        this.colorHover = colorHover;
        this.isHovered = false;
        this.colorFillActive = colorFillActive;
        this.colorStrokeActive = colorStrokeActive;
        this.colorTextActive = colorTextActive;
        this.active = false;
        this.visible = visible;
        this.activable = activable;
    }
    amIFocused(position) {
        return !(!this.visible || undefined === position || position.x < this.x || position.x > this.x + this.w || position.y < this.y || position.y > this.y + this.h);
    }
    click(position) {
        if (!this.amIFocused(position)) {
            return null;
        }
        if (this.activable) {
            this.active = true;
        }
        return this.title;
    }
    hover(position) {
        if (!this.amIFocused(position)) {
            this.isHovered = false;
            return false;
        }
        this.isHovered = true;
        return true;
    }
    unactive() {
        this.active = false;
    }
    setInvisible() {
        this.visible = false;
    }
    setVisible() {
        this.visible = true;
    }
    draw(position) {
        if (!this.visible) {
            return;
        }
        this.hover(position);
        this.ctx.lineWidth = this.lineWidth;
        roundRect(this.ctx, this.x, this.y, this.w, this.h, this.active ? this.colorStrokeActive : this.colorStroke, this.active ? this.colorFillActive : this.isHovered ? this.colorHover : this.colorFill, 5);
        this.ctx.font = this.sizeText.toString() + "px Arial";
        this.ctx.fillStyle = this.active ? this.colorTextActive : this.colorText;
        this.ctx.fillText(this.text, this.xText, this.y + this.sizeText + 6, this.w - 8);
    }
}
