class Resource {
    constructor(quantity, quantityMax, production) {
        this.quantity = quantity;
        this.quantityMax = quantityMax;
        this.production = production;
    }
    getQuantity() {
        return roundNumber(this.quantity, 0);
    }
    getQuantityMax() {
        return roundNumber(this.quantityMax, 0);
    }
    getProduction() {
        return roundNumber(this.production, 2);
    }
    add(quantity) {
        this.quantity += quantity;
        if (this.quantityMax && this.quantity > this.quantityMax) {
            this.quantity = this.quantityMax;
        }
    }
    substract(quantity) {
        this.quantity -= quantity;
        if (this.quantity < 0) {
            this.quantity = 0;
        }
    }
    canAfford(quantity) {
        return this.quantity >= quantity;
    }
    tick(timeElapsed) {
        this.add(this.production * timeElapsed);
    }
    toJSON() {
        throw new Error("Method not implemented.");
    }
}
