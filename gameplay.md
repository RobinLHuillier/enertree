**Ressources principales**

- Energy 
- Matter
- Seeds

**Ressources secondaires**

Correspondent aux batiments (en fait les parties de l'arbre), et coutent de la matière:

- trunk / branch / twigs (emplacements pour les feuilles, fleurs, piments)
- leaves (récupère de l'énergie du soleil)
- flowers (produit lentement des graines)
- roots (stocke l'énergie, transforme lentement l'énergie en matière)
[JN] - champignons (récupère de l'énergie de la lune, produit lentement des graines ? )
- hot peppers / thorns (défense contre les ennemis, qui sinon bouffent ce qui est sur les twigs)
    - hot peppers : repousse périodiquement, mangé par les bugs
    - thorns : ne repousse pas, est détruit peu à peu par les herbivores

**Animals**

*Diurnes*
- bugs (mange les feuilles, les fleurs, les piments, repoussés par les piments)
- bees (pollinisent les fleurs, accèlèrent la production de graines)
- bunnies (mangent les feuilles, les fleurs, repoussés par les thorns)

[JN]
*Nocturnes*
- bugs (mange les feuilles, les fleurs, les piments, repoussés par les piments)
- bats (pollinisent les fleurs, accèlèrent la production de graines)
- hedgehog (mangent les feuilles, les fleurs, repoussés par les thorns)

**Gameplay**

Donc en mécanique principale, on a la récupération d'énergie, transformation en matière, construction de parties de l'arbre, et les graines permettent l'achat d'upgrades, des améliorations d'adn de l'arbre qui améliore les performances de chaque partie de l'arbre. 

[JN] Cycle jour-nuit, pas prioritaire, mais ça peut être sympa. Amène un peu de différence de gameplay.

**Costs**

*tout à réajuster*
- trunk : 10 matter / progression x3
- branch : 5 matter / progression x1.5
- twig : 2 matter / progression x1.5
- leaf : 1 matter / progression x1.5

- flower : 5 matter / progression x1.5
- root : 5 matter / progression x1.5

- mushroom : 5 matter / progression x1.5
- hot pepper : 5 matter / progression x1.5
- thorn : 5 matter / progression x1.5

**Upgrades**







**Idées à creuser**

1. **Tronc / branches / rameaux** 
    - Ramification : Permet au joueur de diviser une branche existante en deux, ce qui double le nombre de feuilles, de fleurs ou de piments qu'elle peut porter.
    - Bark Armor : Les branches résistent mieux aux attaques des herbivores.
    - Nutrient Transport: Augmente la vitesse à laquelle l'énergie et la matière sont réparties dans l'arbre.
    
2. **Feuilles**
    - Photosynthèse accélérée : Augmente la vitesse de production d'énergie.
    - Camouflage : Rend les feuilles moins attrayantes pour les herbivores.
    - Caduque : Les feuilles tombent en automne, économisant de l'énergie, et repoussent au printemps.
    
3. **Fleurs**
    - Pollinisation croisée : Augmente la production de graines lorsqu'une abeille ou une chauve-souris visite la fleur.
    - Parfum attractif : Augmente la fréquence des visites des abeilles et des chauve-souris.
    - Fruits : Après une certaine période, les fleurs se transforment en fruits qui donnent un bonus de matière lorsqu'ils sont récoltés.
    
4. **Racines**
    - Mycorrhize : Les racines s'associent avec des champignons souterrains pour augmenter la transformation de l'énergie en matière.
    - Profondes : Augmente le stockage maximal d'énergie.
    - Résistantes : Les racines peuvent survivre à une certaine quantité de dommages sans perdre leur capacité de stockage.

5. **Champignons** 
    - Symbiose : Augmente le taux de production de graines en échange d'une certaine quantité d'énergie.
    - Spores : Permet aux champignons de se propager à d'autres parties de l'arbre.
    - Bioluminescence : Les champignons produisent de la lumière la nuit, ce qui permet de récupérer un peu d'énergie.

6. **Piments / épines**
    - Poison : Les herbivores qui mangent des piments ou des épines prennent des dégâts au fil du temps.
    - Croissance rapide : Les piments ou les épines repoussent plus rapidement après avoir été mangés.
    - Super épine : Les épines ne sont plus détruites par les herbivores, mais nécessitent plus de matière pour être créées.
